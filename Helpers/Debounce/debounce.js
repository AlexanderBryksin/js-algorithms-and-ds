// Debounce function for delay events

function debounce(func, wait, immediate) {
  let timeout;

  // This is function that is actually executed when
  // the DOM event is triggered
  return function executedFunction() {
    // Store the context and any parameters passed
    let context = this;
    let args = arguments;

    // The function to be called after
    // the debounce elapsed
    let later = function () {
      // null timeout to indicate the debounce ended
      timeout = null;

      // call function now if you did not on leading end
      if (!immediate) func.apply(context, args);
    };

    // Determine if you should call function
    // on the leading or trail end

    let callNow = immediate && !timeout;

    // This will reset the waiting every function execution.
    // This is the step that prevents the function from
    // being executed because it will never reach the
    // inside of the previous setTimeout
    clearTimeout(timeout);

    // Restart the debounce waiting period.
    // setTimeout returns a truthy value (it differs in web vs node)
    timeout = setTimeout(later, wait);

    // Call immediately if you are dong a leading and execution
    if (callNow) func.apply(context, args);
  };
};


let delay = debounce(() => {
  console.log('JS');
}, 1000);

let nodelay = () => {
  console.log('No debounce here');
};
