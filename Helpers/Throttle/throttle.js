// Throttle function

function throttle(func, limit) {
let lastFunc;
let lastRun;

return function () {
  const context = this;
  const args = arguments;

  if (!lastRun) {
    func.apply(context, args);
    lastRun = Date.now();
  } else {
    clearTimeout(lastFunc);
    lastFunc = setTimeout(() => {
      if ((Date.now() - lastRun) >= limit) {
        func.apply(context, args);
        lastRun = Date.now();
      }
    }, limit - (Date.now() - lastRun))
  }
}
}

let maxLimit = throttle(() => {
  console.log('click faster, its doesnt matter');
}, 2000);

let nodelay2 = () => {
  console.log('print as fast i click');
}
