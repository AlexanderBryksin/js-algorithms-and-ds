// Safely inspect nested objects with reduce

let luke = {
  name: "luke skywalker",
  jedi: true,
  parents: {
    father: {
      jedi: true
    },
    mother: {
      jedi: false
    }
  }
}

let han = {
  name: "han solo",
  jedi: false,
  parents: {
    father: {
      jedi: false
    },
    mother: {
      jedi: false
    }
  }
}

let anakin = {
  name: "anakin skywalker",
  jedi: true,
  parents: {
    mother: {
      jedi: false
    }
  }
}

const characters = [luke, han, anakin];

function fatherWasJedi(character) {
  let path = 'parents.father.jedi';
  return path.split('.').reduce((obj, field) => {
    if (obj) {
      return obj[field];
    }

    return false;
  }, character)
}

characters.forEach(character => {
  console.log(character.name + 'father was a ' + fatherWasJedi(character));
})
