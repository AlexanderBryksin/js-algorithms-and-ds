// Reduce Filter and map over Large Databases

const data = [1, 2, 3];

const doubled = data.reduce((acc, value) => {
  acc.push(value * 2);

  return acc;
}, []);

console.log(doubled);

// Map
const doubleMapped = data.map(item => item * 2);
console.log(doubleMapped);

const newData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// Evens numbers
let evens = newData.reduce((acc, value) => {
  if (value % 2 === 0) {
    acc.push(value);
  }

  return acc;
}, []);

console.log(evens);

// Filter
const evenFiltered = newData.filter(item => item % 2 === 0);
console.log(evenFiltered);


// Filtered Map

const filteredMap = newData.filter(value => {
  return value % 2 === 0;
}).map(value => {
  return value * 2;
});

console.log(filteredMap);

// Big data

const bigData = [];
for (let i = 0; i < 1000000; i++) {
  bigData[i] = i;
};

console.time('bigData');
const filteredMapBigData = bigData.filter(value => {
  return value % 2 === 0;
}).map(value => {
  return value * 2;
});
console.timeEnd('bigData');


console.time('bigDataReduce');
const bigDataReduce = bigData.reduce((acc, value) => {
  if(value % 2 === 0) {
    acc.push(value);
  }

  return acc;
}, []);
console.timeEnd('bigDataReduce');
