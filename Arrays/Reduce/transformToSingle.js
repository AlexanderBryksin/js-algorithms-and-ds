// Transform an Array into Single Value using Reduce

const data = [1, 2, 3];


const reducer = (accumulator, item) => {
  return accumulator + item;
};

let initialValue = 0;

let total = data.reduce(reducer, initialValue);

// Total sum
console.log(total);
