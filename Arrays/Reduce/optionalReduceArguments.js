// Use optional reduce arguments

function reducer(accumulator, value, index, array) {
  let intermidiaryValue = accumulator + value;

  if (index === array.length - 1) {
    return intermidiaryValue / array.length;
  }

  return accumulator + value;
}

const data = [1, 2, 3, 3, 4, 5, 3, 1];

let mean = data.reduce(reducer, 0);

console.log(mean);
