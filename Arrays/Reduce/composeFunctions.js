// Compose functions

function increment(input) {
  return input + 1;
}

function decrement(input) {
  return input - 1;
}

function double(input) {
  return input * 2;
}

function halve(input) {
  return input / 2;
}

let initialValue = 1;

// let incremented_value = increment(initialValue);
// let doubled_value = double(incremented_value);
// let final_value = decrement(doubled_value);

// function transform(input) {
//   return ((input + 1) * 2) - 1;
// }
//
// let final_value = transform(initialValue);
//
// console.log(final_value);


const pipeline = [
  increment,
  double,
  decrement
];

let final_value = pipeline.reduce((acc, fn) => {
  return fn(acc);
}, initialValue);

console.log(final_value);
