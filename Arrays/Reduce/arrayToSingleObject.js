// Recuce array to single object


const votes = [
  "React",
  "Vue",
  "Angular",
  "Vue",
  "Vue",
  "Vue",
  "React",
  "React",
  "Angular",
];

let initialValue = {};

const reducer = (accumulator, item) => {
  if (!accumulator[item]) {
    accumulator[item] = 1;
  } else {
    accumulator[item]++;
  }
  return accumulator;
};

votes.reduce(reducer, initialValue);

// Object
console.log(initialValue);
