// Binary Search

function binarySearch(list = [], item) {
  let min = 0;
  let max = list.length - 1;
  let center;

  while (min <= max) {
    center = Math.floor((min + max) / 2);

    if (list[center] === item) {
      return center;
    } else {
      if (list[center] < item) {
        min = center + 1;
      } else {
        max = center - 1;
      }
    }
  }

  return -1;
}

binarySearch([2, 6, 90, 120, 250], 120);
console.log(binarySearch([2, 6, 90, 120, 250], 120));

const binarySeacrh2 = (list = [], item) => {
  let min = 0;
  let max = list.length - 1;
  let guess;

  while (min <= max) {
    guess = Math.floor((min + max) / 2);

    if (list[guess] === item) {
      return guess;
    } else {
      if (list[guess] < item) {
        min = guess + 1;
      } else {
        max = guess - 1;
      }
    }
  }

  return -1;
};

console.log(binarySeacrh2([2, 6, 90, 120, 250], 120));
