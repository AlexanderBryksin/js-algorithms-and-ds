// Transform array to sorted array wthout diplicates

const array = [12, 5, 7, 10, 5, 100, 10, 100, 70, 250, 500];
// Solution 1
function uniqSort1(array = []) {
  return Array.from(new Set(array)).sort((a, b) => a - b);
}

console.log(uniqSort1(array));

// Solution 2
function uniqSort2(array = []) {
  const breadCrumbs = {};
  const result = [array[0]];

  for (let i = 1; i < array.length; i++) {
    // Start loop at 1 as element 0 can never be a duplicate
    if (!breadCrumbs[array[i]]) {
      result.push(array[i]);
      breadCrumbs[array[i]] = true;
    }
  }

  return result.sort((a, b) => a - b);
}

console.log(uniqSort2(array));
