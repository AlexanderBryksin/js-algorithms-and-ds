function computeFactorial(number) {
  let result = 1;

  for (let i = 2; i <= number; i++) {
    console.log(`result = ${result} * ${i} (${result * i})`);
    result *= i;
  }
  return result;
}

computeFactorial(4);

// Recursive

function recursiveFactorial(num) {
  if (num === 1) {
    console.log('Hitting base case');
  } else {
    console.log(`returning ${num} * computeFactorial(${num - 1})`);
    return num * computeFactorial(num - 1);
  }
}

console.log(recursiveFactorial(5));
recursiveFactorial(5);
