// Memoization Exercise Solutions

// Basic Soluition

const cache = {};

const memoTimes10 = n => {
  if (n in cache) {
    console.log('Fetching from cache' + n);
    return cache[n];
  } else {
    console.log('Calculating result');
    let result = n * 10;
    return result;
  }
};

console.log(memoTimes10(10));
// Memoization with Closure

const memoizedClosureTimes10 = () => {
  const cache = {};
  return n => {
    if (cache[n]) {
      console.log('Fetching from cache' + n);
      return cache[n];
    } else {
      let result = n * 10;
      cache[n] = result;
      return result;
    }
  };
};

const times10 = n => n * 10;
// Generic Memoize

const genericMemoize = cb => {
  const cache = {};

  return (...args) => {
    if (cache[args]) {
      return cache[args];
    } else {
      const result = cb(...args);
      cache[args] = result;
      return result;
    }
  };
};

const memoize10 = genericMemoize(times10);
console.log(memoize10(10));
