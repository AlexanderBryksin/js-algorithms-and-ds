// Linear Search

// Solution 1
function linearSearch1(list = [], item) {
  const result = list.indexOf(item);
  console.log(result);
  return result;
}

linearSearch1([2, 6, 90, 120, 250], 120);
console.log(linearSearch1([2, 6, 90, 120, 250], 120));

// Solution 2
function linearSearch2(list = [], item) {
  let index = -1;
  list.forEach((listItem, i) => {
    if (listItem === item) {
      index = i;
      console.log(index);
    }
  });
  return index;
}

linearSearch2([2, 6, 90, 120, 250], 120);
console.log(linearSearch2([2, 6, 90, 120, 250], 120))


function nightOwl(payload) {

  return function
}

const arr = [1,2,3,4];
arr.prototype