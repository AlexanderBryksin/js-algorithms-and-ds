// binary search tree


// Node of tree
class Tree {
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }

  // Insert data recursion
  insert(data) {
    if (this.left && data < this.data) {
      this.left.insert(data);
    } else if (data < this.data) {
      this.left = new Node(data);
    } else if (this.right && data > this.data) {
      this.right.insert(data);
    } else if (data > this.data) {
      this.right = new Node(data);
    }
  }

  // Contains recursion
  contains(data) {
    if (data === this.data) {
      return this;
    }

    if (this.data < data && this.right) {
      this.right.contains(data);
    } else if (this.data > data && this.left) {
      this.left.contains(data);
    }

    return null;
  }

}


//

class Tree2 {
  constructor(data) {
    this.left = null;
    this.right = null;
    this.data = data;
  }

  insert(data) {
    if (this.left && data < this.data) {
      this.left.insert(data);
    } else if (data < this.data) {
      this.left = new Node(data);
    } else if (this.right && data > this.data) {
      this.right.insert(data);
    } else if (data > this.data) {
      this.right = new Node(data);
    }
  }

  contsains(data) {
    if (this.data === data) {
      return this;
    }

    if (this.right && data > this.data) {
      this.left.contains(data)
    } else if (this.left && data < this.data) {
      this.left.contsains(data)
    }
  }

}

const tree2 = new Tree(100);
tree2.insert()
tree2.insert({id: 170})


console.log(tree2);
