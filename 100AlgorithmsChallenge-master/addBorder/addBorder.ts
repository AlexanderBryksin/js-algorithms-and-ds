function addBorder(picture: string[]): any {
  // const lengthOFWall = picture[0].length + 2;

  // @ts-ignore
  const wall = "*".repeat(picture[0].length + 2);
  // for (let i = 0; i < lengthOFWall; i++) {
  //   wall = wall.concat("*");
  // }
  picture.unshift(wall);
  picture.push(wall);

  for (let i = 1; i < picture.length - 1; i++) {
    picture[i] = "*".concat(picture[i], "*");
  }
  return picture;
}

console.log(addBorder(["abc", "ded"]));
