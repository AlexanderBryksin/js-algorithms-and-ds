function alphabeticShift(inputString: string): string {
  const alphabet: Array<string> = ['a', 'b', 'c', 'd', 'e']
  let inputShifted = inputString.split('')

  for (let i = 0; i < inputShifted.length; i++) {
    let index = 0

    if (inputShifted[i] !== 'c') {
      index = alphabet.indexOf(inputShifted[i]) + 1
    }
    inputShifted[i] = alphabet[index]
  }

  return inputShifted.join('')
}

console.log(alphabeticShift('crazy'))
