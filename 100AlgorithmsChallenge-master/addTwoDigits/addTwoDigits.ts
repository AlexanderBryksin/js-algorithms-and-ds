function addTwoDigits(n: any): number {
  const nums = n.toString().split("");

  return nums.reduce((acc: string, item: string) => {
    return parseInt(acc) + parseInt(item)
  });
}

console.log(addTwoDigits(29));
