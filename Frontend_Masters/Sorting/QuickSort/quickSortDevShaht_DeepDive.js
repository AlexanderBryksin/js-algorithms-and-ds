// QuickSort Deep Dive

const arr = [1, 20, 12, 7, 5, 57, 150, 250, 500, 500];

function partition(arr, low, hi) {
  const pivotPosition = Math.floor(Math.random() * arr.length);
  const pivot = arr[pivotPosition];

  while (hi >= low) {
    while (arr[hi] > pivot) {
      hi--;
    }
    while (arr[low] < pivot) {
      low++;
    }
    if (hi >= low) {
      const temp = arr[low];
      arr[low] = arr[hi];
      arr[hi] = temp;
      hi--;
      low++;
    }
  }

  return low;
}

partition(arr, 0, arr.length - 1);


function quickSort(arr, low = 0, hi = arr.length - 1) {
  if (low < hi) {
    const index = partition(arr, low, hi);
    quickSort(arr, low, index - 1);
    quickSort(arr, index, hi);
  }
  return arr;
}

quickSort(arr);

console.log(quickSort(arr));





// Swap function
function swap(a, b, arr) {
  const temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;

  return arr;
}

//

const newArray = [];
let counter = 0;
function recursion(array = []) {
  if (array.length < 1) {
    return array;
  } else {
    counter++;
    newArray.push(array.shift());
    recursion(array);
  }
}

recursion(arr);
console.log(counter);
let value = 0;
let value2 = 0;
function rec(counter) {
  if (counter < 1) {
    return counter;
  }
  value++;
  return rec(counter - 1);
  value2++;
}

rec(10);
console.log(value);
