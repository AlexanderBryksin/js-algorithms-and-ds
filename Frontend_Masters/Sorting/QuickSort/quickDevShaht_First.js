// Dev chaht QUick Sort
// Math.floor(Math.random() * arr.length) - more stable then arr[0] or arr[arr.length - 1]
const arr = [1, 20, 12, 7, 5, 57, 150, 250, 500];

function quickSort(arr) {
  if (arr.length < 2) {
    return arr;
  } else {
    const pivot = arr[Math.floor(Math.random() * arr.length)];
    const less = arr.filter(value => value < pivot);
    const greater = arr.filter(value => value > pivot);
    return [...quickSort(less), pivot, ...quickSort(greater)];
  }
}

quickSort(arr);

console.log(quickSort(arr));
