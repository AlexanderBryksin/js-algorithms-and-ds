// Quick Sort

const arr = [1, 12, 5, 7, 57, 120, 10, 150, 250, 500];

// function quickSort(array = [], low = 0, high = array.length - 1) {
//   if (low === undefined) low = 0;
//   if (high === undefined) high = array.length - 1;
//
//   if (low < high) {
//     let p = partition(array, low, high);
//
//     quickSort(array, low, p - 1);
//     quickSort(array, p + 1, high);
//   }
//
//   if (high - low === array.length - 1) return array;
// }
//
// quickSort(arr);
// console.log(quickSort(arr));
//
//
// function partition(array, low = 0, high = array.length - 1) {
//   let pivot = array[high];
//   let pivotLoc = low;
//
//
//   for (let i = low; i < high; i++) {
//     if (array[i] <= pivot) {
//       swap(array, i, pivotLoc);
//       pivotLoc++;
//     }
//   }
//   swap(array, pivotLoc, high);
//   return pivotLoc;
// }
//
// partition(arr);
// console.log(partition(arr));
//
function swap(arr, i1, i2) {
  if (i1 === i2) return;
  let temp = arr[i1];
  arr[i1] = arr[i2];
  arr[i2] = temp;
  console.log('swapped', arr[i1], arr[i2], 'in', arr);
  return arr;
}

function partition2(array = [], low = 0, high = array.length - 1) {
  let pivot = array[high];
  let pivotLoc = low;

  for (let i = low; i < high; i++) {
    if (array[i] <= pivot) {
      swap(array, i, pivotLoc);
      pivotLoc++;
    }
  }
  swap(array, pivotLoc, high);
  return pivotLoc;
}

function quickSort2(array = [], low = 0, high = array.length - 1) {
  if (low === undefined) low = 0;
  if (high === undefined) high = array.length - 1;

  if (low < high) {
    let p = partition2(array, low, high);

    quickSort2(array, low, p - 1);
    quickSort2(array, p + 1, high);
  }

  if (high - low === array.length - 1) return array;
}

quickSort2(arr);
console.log(quickSort2(arr));


///
function quickSort3(array = [], low = 0, high = array.length - 1) {
  if (low === undefined) low = 0;
  if (high === undefined) high = array.length - 1;

  if (low < high) {
    let p = partition3(array, low, high);

    quickSort3(array, low, p - 1);
    quickSort3(array, p + 1, high);
  }
  if ((high - low) === array.length - 1) return array;
}

function partition3(array = [], low = 0, high = array.length - 1) {
  let pivot = array[high];
  let pivotLoc = low;

  for (let i = low; i < high; i++) {
    if (array[i] <= pivot) {
      swap(array, pivotLoc, i);
      pivotLoc++;
    }
  }
  swap(array, high, pivotLoc);
  return pivotLoc;
}

const array2 = [1, 10, 5, 7, 12, 15, 2, 100, 57, 250];

quickSort3([1, 10, 5, 7, 12, 15, 2, 100, 57, 250]);
console.log(quickSort3(array2));
