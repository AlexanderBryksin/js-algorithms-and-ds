// Recursive reverse
// Write a function "recursiveReverse" that takes an array and uses recursion to return its contents in reverse

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// const recursiveReverse = function (arr) {
//   const reversedArr = [];
//
//   const addItems = function (orderedArr) {
//     if (orderedArr.length < 1) {
//       return arr;
//     } else {
//       reversedArr.push(orderedArr.pop());
//       addItems(arr);
//     }
//     return;
//   };
//
//   addItems(arr);
//   console.log(reversedArr);
//   return reversedArr;
// }
//
// recursiveReverse(arr);
// console.log(recursiveReverse(arr));
///
//
function recursiveReverse(arr) {
  const reversedArr = [];

  function addItems(orderedArr) {

    if (orderedArr.length > 0) {
      reversedArr.push(orderedArr.pop());
      addItems(orderedArr);
    }
    return;
  }

  addItems(arr);
  console.log(reversedArr);
  return reversedArr;
}

recursiveReverse(arr);
console.log(recursiveReverse(arr));
