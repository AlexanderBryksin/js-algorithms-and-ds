// Recursive Multiplier
// Write a function 'recursiveMultiplier' that takes two arguments, 'arr and num',
// and multiplies each arr value by num and returns an array of the values.

const arr = [1, 2, 3];

function recursiveMultiplier(arr, num) {
  if (arr.length === 0) {
    return arr;
  } else {
    let last = arr.pop();
    recursiveMultiplier(arr, num);
    arr.push(last * num);
    return arr;
  }
}

recursiveMultiplier(arr, 2);
console.log(recursiveMultiplier(arr, 2));
