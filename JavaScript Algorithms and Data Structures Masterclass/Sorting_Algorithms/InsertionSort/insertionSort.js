// Insertion Sort

function insertionSort(arr) {
  let currentVal;
  for (let i = 0; i < arr.length; i++) {
    currentVal = arr[i];
    for (let j = i-1; j >= 0 && arr[j] > currentVal ; j--) {
      arr[j+1] = arr[j]
    }
    arr[i+1] = currentVal;
  }
  console.log(arr);
  return arr;
}

const arr = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];

insertionSort(arr);

