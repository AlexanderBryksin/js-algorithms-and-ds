// bubbleSort
const arr = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];

function bubbleSort(arr = []) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < i-1; j++) {
      if (arr[j] > arr[j+1]) {
        let temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
      }
    }
  }
  console.log(arr);
  return arr;
}

bubbleSort(arr);

const arr2 = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];

function bubbleSort2(arr) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < i-1; j++) {
      if (arr[j] > arr[j+1]) {
        [arr[j], arr[j+1]] = [arr[j+1], arr[j]];
      }
    }
  }
  console.log(arr);
  return arr;
}

bubbleSort2(arr2);

function bubbleSortAlgo(arr = []) {
  for (let i = arr.length; i > 0; i--) {
    for (let j = 0; j < i-1; j++) {
      if (arr[j] > arr[j+1]) {
        let temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
      }
    }
  }
  console.log(arr);
  return arr
}
const arr3 = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];

bubbleSortAlgo(arr3);
