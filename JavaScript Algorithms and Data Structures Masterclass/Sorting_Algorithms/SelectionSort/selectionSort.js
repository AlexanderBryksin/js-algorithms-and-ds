// Selection Sort

function selectionSort(arr = []) {
  for (let i = 0; i < arr.length; i++) {
    let indexOfMin = i;

    for (let j = i+1; j < arr.length; j++) {
      if (arr[j] < arr[indexOfMin]){
        indexOfMin = j;
      }
    }
    if (i !== indexOfMin) {
      let temp = arr[indexOfMin];
      arr[indexOfMin] = arr[i];
      arr[i] = temp;
    }
  }
  console.log(arr);
  return arr;
}

const arr = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];
selectionSort(arr);
