// Merge Sort
const arr = [5, 1, 7, 12, 10, 100, 57, 120, 150, 250];

function mergeSortAlgo(arr = []) {
  if (arr.length <= 1) {
    return arr;
  }

  const center = Math.floor(arr.length / 2);
  const left = arr.slice(0, center);
  const right = arr.slice(center);

  return merge(mergeSortAlgo(left), mergeSortAlgo(right))
}

function merge(left = [], right = []) {
  const results = [];

  while (left.length && right.length) {
    if (left[0] < right[0]) {
      results.push(left.shift())
    } else {
      results.push(right.shift())
    }
  }
  return [...results, ...left, ...right];
}

merge([2,4,1], [3,7,5]);
console.log(merge([2,4,1], [3,7,5]));

console.log(mergeSortAlgo(arr));

function mergeShort(left = [], right = []) {
  const results = [];

  while (left.length && right.length) {
    left[0] < right[0] ? results.push(left.shift()) : results.push(right.shift())
  }
  return [...results, ...left, ...right];
}

function mergeLong(left = [], right = []) {
  const results = [];
  let i = 0;
  let j = 0;

  while (i < left.length && j < right.length) {
    if (left[i] < right[j]) {
      results.push(left[i]);
      i++;
    } else {
      results.push(right[j]);
      j++;
    }
  }
  while (i < left.length) {
    results.push(left[i])
    i++;
  }
  while (j < right.length) {
    results.push(right[j])
    j++;
  }
  return results;
}

