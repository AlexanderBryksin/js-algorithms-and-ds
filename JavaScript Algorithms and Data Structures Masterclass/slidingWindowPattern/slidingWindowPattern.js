// sliding Window Pattern

// Solution Naive O(N^2)
function maxSubArraySumNaive(arr = [], num) {
  if (num > arr.length) {
    return null;
  }

  let max = -Infinity;
  for (let i = 0; i < arr.length - num + 1; i++) {
    let temp = 0;
    for (let j = 0; j < num; j++) {
      temp += arr[i + j];
    }
    if (temp > max) {
      max = temp;
    }
  }
  return max;
}

maxSubArraySumNaive([1,2,3,4,5], 2);
console.log(maxSubArraySumNaive([1,2,3,4,5], 2));

// Solution O(N)
function maxSubArrayFast(arr = [], num) {
  let maxSum = 0;
  let tempSum = 0;

  if (arr.length < num) {
    return null;
  }
  for (let i = 0; i < num; i++) {
    maxSum += arr[i];
  }

  tempSum = maxSum;
  for (let i = num; i < arr.length; i++) {
    tempSum = tempSum - arr[i - num] + arr[i];
    maxSum = Math.max(maxSum, tempSum);
  }

  return maxSum;
}

maxSubArrayFast([1,2,3,4,5], 3);
console.log(maxSubArrayFast([1,2,3,4,5], 2));


// By functions
const numbersSum = (arr = [], num) => {
  const readyArr = arr
    .sort((a, b) => b - a)
    .splice(0, num)
    .reduce((acc, value) => {
      return acc += value
    })

  return readyArr
}

numbersSum([1,2,3,4,5], 3);
console.log(numbersSum([1,2,3,4,5], 2));
