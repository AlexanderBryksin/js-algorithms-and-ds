class Node {
  constructor(value) {
    this.value = value;
    this.right = null;
    this.left = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(value) {
    const newNode = new Node(value);
    if (!this.root) return this.root = newNode;
    let current = this.root;
    while (true) {
      if (value < current.value) {
        if (!current.left) return current.left = newNode;
        current = current.left;
      }
      if (value > current.value) {
        if (!current.right)  return current.right = newNode;
        current = current.right;
      }
    }
  }


  find(value) {
    if (!this.root) return false;
    let current = this.root;

    while (current) {
      if (value === current.value) return current;
      if (value < current.value) {
        current = current.left
      } else if (value > current.value) {
        current = current.right;
      } else {
        return current;
      }
    }
  }


  BFS() {
    let current = this.root;
    let data = [];
    let queue = [];
    queue.push(this.root);
    while (queue.length) {
      current = queue.shift();
      data.push(current.value);
      if (current.left) queue.push(current.left);
      if (current.right) queue.push(current.right);
    }
    return data
  }

  DFSPreOrder() {
    let data = [];
    function traverse(node) {
      data.push(node.value);
      if (node.left) traverse(node.left);
      if (node.right) traverse(node.right);
    }
    traverse(this.root);
    return data;
  }

  DFSPostOrder() {
    let data = [];
    function traverse(node) {
      if (node.left) traverse(node.left);
      if (node.right) traverse(node.right);
      data.push(node.value);
    }
    traverse(this.root);
    return data;
  }

  DFSInOrder() {
    let data = [];
    function traverse(node) {
      if (node.left) traverse(node.left);
      data.push(node.value);
      if (node.right) traverse(node.right);
    }
    traverse(this.root);
    return data;
  }

}



const tree = new BinarySearchTree();
tree.insert(20);
tree.insert(10);
tree.insert(30);
tree.insert(25);
tree.insert(35);

console.log(tree.DFSInOrder());