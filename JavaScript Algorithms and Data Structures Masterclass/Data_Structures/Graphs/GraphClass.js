class GraphClass {
  constructor() {
    this.list = {};
  }

  addVertex(vertex) {
    if (!this.list[vertex]) {
      this.list[vertex] = vertex;
    }
  }

  addEdge(vertex1, vertex2) {
    this.list[vertex1].push(vertex2);
    this.list[vertex2].push(vertex1);
  }

  removeEdge(vertex1, vertex2) {
    this.list[vertex1] = this.list[vertex1].filter(v => v !== vertex2);
    this.list[vertex2] = this.list[vertex2].filter(v => v !== vertex1);
  }

  removeVertex(vertex) {
    while (this.list[vertex].length) {
      const secondVertex = this.list[vertex].pop();
      this.removeEdge(vertex, secondVertex);
    }
    delete this.list[vertex];
  }

}



const g = new GraphClass();


function coolDeikstra() {
  const cache = {};

  return function (...args) {
    if (cache[args]) {
      
    }
  }
}

