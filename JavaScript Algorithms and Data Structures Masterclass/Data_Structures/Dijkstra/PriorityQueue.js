class PriorityQueue {
  constructor() {
    this.values = [];
  }

  enqueue(val, priority) {
    this.values.push({val, priority});
    this.sort()
  }
  dequeue() {
    return this.values.shift();
  }

  sort() {
    this.values.sort((a,b) => a.priority - b.priority);
  }

}



const q = new PriorityQueue();
q.enqueue('A', 3);
q.enqueue('B', 5);
q.enqueue('C', 2);
q.enqueue('D', 20);

console.log(q.values);
