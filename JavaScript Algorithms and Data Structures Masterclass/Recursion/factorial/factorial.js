// factorial

// Iterative

function factorialIter(num) {
  let total = 1;

  for (let i = num; i > 0; i--) {
    total *= i;
  }
  return total;
}

factorialIter(10);
console.log(factorialIter(10));

// Recursive

function factorial(num) {
  if (num === 1) return 1;
  return num * factorial(num - 1);
}

factorial(10);
console.log(factorial(7));

function factorialRec(num) {
  let total = 1;

  if (num === 1) {
    return total;
  } else {
    total = num * factorialRec(num - 1);
    return total;
  }
}

factorialRec(10);
console.log(factorialRec(10));
// Night Owl New 1 Top





const recursion = (value) => {
  let acc = 0;

  if (value === 0) {
    return acc;
  } else {
    return value + recursion(value - 1)
  }
}

recursion(10);
console.log(recursion(10));
