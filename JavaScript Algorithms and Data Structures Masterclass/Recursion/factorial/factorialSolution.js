// factorial solution

function factorial(num) {
  if (num < 1) return 0;
  if (num === 1) return 1;

  return num * factorial(num - 1);
}

factorial(10);
console.log(factorial(10));
