// collectStrings

const obj = {
  stuff: "foo",
  data: {
    val: {
      thing: {
        info: "bar",
        moreInfo: {
          evenMoreInfo: {
            weMadeIt: "baz"
          }
        }
      }
    }
  }
}

//collectStrings Solution: Helper Method Recursion Version
function collectStrings1(obj) {
  let stringsArr = [];

  function gatherStrings(o) {
    for(let key in o) {
      if(typeof o[key] === 'string') {
        stringsArr.push(o[key]);
      }
      else if(typeof o[key] === 'object') {
        return gatherStrings(o[key]);
      }
    }
  }

  gatherStrings(obj);

  return stringsArr;
}

collectStrings1(obj) // ["foo", "bar", "baz"])

//collectStrings Solution: Pure Recursion Version
function collectStrings2(obj) {
  let stringsArr = [];
  for(let key in obj) {
    if(typeof obj[key] === 'string') {
      stringsArr.push(obj[key]);
    }
    else if(typeof obj[key] === 'object') {
      stringsArr = stringsArr.concat(collectStrings2(obj[key]));
    }
  }

  return stringsArr;
}

collectStrings2(obj) // ["foo", "bar", "baz"])
