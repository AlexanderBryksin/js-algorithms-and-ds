// Recursion

counter = 0;
value = 0;
function recursion(counter) {
  if (counter <= 10) {
    value++;
    return recursion(counter + 1);
  }
  console.log(value);
  return value;
}

recursion(counter);

let num = 0;
const recurs = (value) => {
  if (value === 0) {
    return;
  } else {
    num++;
    recurs(value - 1);
  }
}

recurs(10);
console.log(num);

const name = 'Recursion';
