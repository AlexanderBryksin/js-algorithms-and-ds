// Map char function
const charCount = (string = '') => {
  const mapChar = {};

  for (let char of string) {
    char = char.toLowerCase();
    if (mapChar[char]) {
      mapChar[char]++;
    } else {
      mapChar[char] = 1;
    }
  }
  return mapChar;
};


console.log(charCount('charcountCCCCasd'));


// Second way


let myPromise = new Promise((resolve, reject) => {
 let cool = true;
  if (cool) {
    resolve('Success')
  } else {
    reject('Sorry')
  }
  resolve('Nice');
});

myPromise.then(data => {
  console.log(data);
}).catch(e => {
  console.log(e);
})



