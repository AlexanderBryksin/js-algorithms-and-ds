// addUpTo find Sum

function addUpTo(n) {
  let total = 0;
  for (let i = 0; i <= n; i++) {
    total += i;
  }
  return total;
}

function addUpTo2(n) {
  return (n * (n + 1)) / 2;
}

console.time('addUp');
addUpTo(1000000);
console.timeEnd('addUp');

console.time('addUp2');
addUpTo2(1000000);
console.timeEnd('addUp2');
// addUp: 3.823974609375ms
// addUp2: 0.062744140625ms
