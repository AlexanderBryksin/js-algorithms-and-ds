function fib(n) {
  if (n <= 2) return;

  return fib(n-1) + fib(n-2);
}


function memoize(fn) {
  const cache = {};

  return (...args) => {
    if (cache[args]) return cache[args];
    const result = fn.apply(this, args);
    cache[args] = result;
    return result
  }
}

function memoizedFib(n, memo = []) {
  if (memo[n] !== undefined) return memo[n];
  if (n <= 2) return 1;
  let res = fib(n-1, memo) + fib(n-2, memo);
  memo[n] = res;
  return res;
}

function tabulationFib(n) {
  if (n <= 2) return 1;
  let fibNums = [0,1,1];
  for (let i = 3; i < n; i++) {
    fibNums[i] = fibNums[i-1] + fibNums[i-2];
  }
  return fibNums;
}



const memFib = memoize(fib);

console.time('fib');
fib(20);
console.timeEnd('fib');

console.time('mem');
memFib(20);
console.timeEnd('mem');

console.time('fibMem');
memoizedFib(20);
console.timeEnd('fibMem');

console.time('tab');
tabulationFib(20);
console.timeEnd('tab');