function memoize(fn) {
  const cache = {};

  return function(...args) {
    if (cache[args]) {
      return cache[args];
    }
    const result = fn.apply(this, args);
    cache[args] = result;

    return result;
  };
}

function fib(num) {
  if (num < 2) return num;

  return fib(num - 1) + fib(num - 2);
}

const memFib = memoize(fib);

console.log(memoize(fib)(10));

//=============================================
console.log('JS Cool !');
