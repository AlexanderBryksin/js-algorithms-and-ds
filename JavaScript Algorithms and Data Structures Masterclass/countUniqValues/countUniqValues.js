// count Uniq Values function

const arr = [1,2,2,5,7,7,99,150];
// Solution 1
function uniqValues(arr) {
  const mapChar = {};

  for (let char of arr) {
    if (mapChar[char]) {
      mapChar[char]++;
    } else {
      mapChar[char] = 1;
    }
  }
  console.log(Object.keys(mapChar).length);
  return Object.keys(mapChar).length;
}

uniqValues(arr);
console.log(uniqValues(arr));

// Solution 2
function uniqValuesSet(arr) {
  return new Set(arr).size;
}

uniqValuesSet(arr);
console.log(uniqValuesSet(arr));
