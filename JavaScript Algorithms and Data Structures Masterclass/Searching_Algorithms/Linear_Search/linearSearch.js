// Linear Search Algorithm

function linearSearch(arr, value) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === value) return i;
  }
  return -1;
}

console.log(linearSearch([1,2,3,4,5], 5));
console.log(linearSearch([100], 100));
const arr = [1,2,3,4,5,'JS'];

