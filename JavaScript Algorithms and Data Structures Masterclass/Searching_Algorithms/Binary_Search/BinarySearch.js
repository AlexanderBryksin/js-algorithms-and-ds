// Binary Search O(log N) - O(1)

function binarySearch(arr, elem) {
  let start = 0;
  let end = arr.length - 1;
  let middle = Math.floor((start + end) / 2);

  while (arr[middle] !== elem && start <= end) {
    if (elem < arr[middle]) {
      end = middle - 1;
    } else {
      start = middle + 1;
    }
    middle = Math.floor((start + end) / 2);
  }
  if (arr[middle] === elem) {
    console.log(start, middle, end);
    return middle;
  }

  return -1;
}

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
binarySearch(arr, 8);
console.log(binarySearch(arr, 7));


const nightOwl = (fontFamily) => {
  const font = {
    name: fontFamily
  }
  return font
}











