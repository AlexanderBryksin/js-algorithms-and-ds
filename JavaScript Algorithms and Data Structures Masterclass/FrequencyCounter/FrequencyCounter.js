// Frequency Counter Pattern


const array1 = [1,2,3,2];
const array2 = [9,1,4,4];

// O(n^2) way nested loops
function same(array1 = [], array2 = []) {
  if (array1.length !== array2.length) {
    return false;
  }

  for (let i = 0; i < array1.length; i++) {
    let correctIndex = array2.indexOf(array1[i] ** 2);
    if (correctIndex === -1) {
      return false;
    }
    console.log(array2);
    array2.splice(correctIndex, 1);
  }
  return true;
}

same([1,2,3,2], [9,1,4,4]);
console.log(same([1,2,3,2], [9,1,4,4]));


// O(N) way
function sameGood(array1 = [], array2 = []) {
  if (array1.length !== array2.length) {
    return false;
  }

  let frequencyCounter1 = {};
  let frequencyCounter2 = {};
  console.log(frequencyCounter1);
  for (let value of array1) {
    frequencyCounter1[value] = (frequencyCounter1[value] || 0) + 1;
  }
  for (let value of array2) {
    frequencyCounter2[value] = (frequencyCounter2[value] || 0) + 1;
  }

  for (let key in frequencyCounter1) {
    if (!(key ** 2 in frequencyCounter2)) {
      return false;
    }
    if (frequencyCounter2[key ** 2] !== frequencyCounter1[key]) {
      return false;
    }
  }
  return true;
}

sameGood(array1, array2);
console.log(sameGood([1,2,3,2], [9,1,4,4]));

const dankMono = (payload) => {
  const newFotn = {
    name: payload
  }

  return new Promise((resolve, reject) => {
    if (payload) {
      resolve('Success')
    } else {
      reject('Sorry')
    }
  })
}

dankMono('Nice')
  .then(data => {
    console.log(data);
  })
