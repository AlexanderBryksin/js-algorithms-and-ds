// same Frequency

function sameFrequency(num1, num2) {
  return clearNumber(num1) === clearNumber(num2);
}
function clearNumber(num) {
  return num
    .toString()
    .split('')
    .sort()
    .join('')
}

//
// function clearNumber(num) {
//   return parseInt(num
//     .toString()
//     .split('')
//     .sort()
//     .join(''))
// }
sameFrequency(1211, 1121);

console.log(sameFrequency(1211, 1121));


//
function areThereDuplicates() {
  return new Set(arguments).size !== arguments.length;
}
